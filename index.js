$(document).ready(function() {
  $("#uidError").hide();
  $("#gradError").hide();
  $("#quoteError").hide();
  $("#citeError").hide();
});

function validate() {
  if (
    document.myForm.uid.value == "" ||
    document.myForm.uid.value.length <= 2
  ) {
    $("#uidError").show();
    return false;
  } else if (
    document.myForm.quote.value == "" ||
    document.myForm.quote.value <= 10
  ) {
    $("#uidError").hide();
    $("#quoteError").show();
    return false;
  } else if (
    document.myForm.quoteCite.value == "" ||
    document.myForm.quoteCite.value <= 2
  ) {
    $("#uidError").hide();
    $("#quoteError").hide();
    $("#citeError").show();
    return false;
  } else {
    $("#uidError").hide();
    $("#gradError").hide();
    $("#quoteError").hide();
    $("#citeError").hide();
    return true;
  }
}


function switchCar(){
    if($('input[name="car"]').is(':checked'))
    {
        document.myForm.car.value = "Yes"
    } else {
        document.myForm.car.value = "No"
    }
}

function switchCse(){
    if($('input[name="cse"]').is(':checked') )
    {
        document.myForm.cse.value = "Yes"
    } else {
        document.myForm.cse.value = "No"
    }
}